var gulp = require('gulp');
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var paths = {
  coffee: 'coffee/*.coffee'
};

gulp.task('coffee', function() {
  // Minify and copy all JavaScript (except vendor coffee)
  return gulp.src(paths.coffee)
    .pipe(coffee())
    .pipe(concat('snake.js'))
    .pipe(gulp.dest('js'))
    .pipe(uglify())
    .pipe(concat('snake.min.js'))
    .pipe(gulp.dest('js'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.coffee, ['coffee']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['coffee', 'watch']);
