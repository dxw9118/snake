#variable
size = [500,500]

grid = [50,50]

scale = size[0]/grid[0]

snakes = []

speed = 100;

toPos = (position) ->
  [position[0]*scale,position[1]*scale]

food = null

setFood = ->
  food = [parseInt(Math.random() * grid[0]),parseInt(Math.random() * grid[1])]

canvas = document.getElementsByTagName('canvas')[0]
ctx = canvas.getContext '2d'

drawFood = ->
  ctx.clearRect 0,0,size[0],size[1]
  ctx.fillStyle = '#000'
  _foot = toPos food
  ctx.fillRect _foot[0],_foot[1],scale,scale

setFood()

#snakes
class Snake
  constructor : ( @name, @type ,@color) ->

  bindDirection : (code) ->

    if @type == 'right'
      @keys = [83,65,87,68]
    else
      @keys = [40,37,38,39]

    if code == @keys[0] && @directionPre != "up"
      @direction = "down"
    else if code == @keys[1] && @directionPre != "right"
      @direction = "left"
    else if code == @keys[2] && @directionPre != "down"
      @direction = "up"
    else if code == @keys[3] && @directionPre != "left"
      @direction = "right"

  draw : ->
    ctx.fillStyle = @color
    for val,i in @.body
      _val = toPos val
      ctx.fillRect _val[0],_val[1],scale,scale

  check : ->
    _snake = @body[0]
    if _snake[0] >= 0 && _snake[0] < grid[0] && _snake[1] >= 0 && _snake[1] < grid[0]
      for snake in snakes
        for val,i in snake.body
          if ( snake.name != @name || i > 0 ) && _snake[0] == val[0] && _snake[1] == val[1]
            @lose = true 
    else
      @lose = true

  eatFood : ->
    setFood()

  run : ->
    if @direction == 'up'
      _step = [@body[0][0],@body[0][1] - 1]
    else if @direction == 'right'
      _step = [@body[0][0] + 1,@body[0][1]]
    else if @direction == 'down'
      _step = [@body[0][0],@body[0][1] + 1]
    else if @direction == 'left'
      _step = [@body[0][0] - 1,@body[0][1]]

    if @directionPre != @direction
        @directionPre = @direction
      @.body.unshift _step
      if @.body[0][0] == food[0] && @.body[0][1] == food[1]
        @eatFood()
      else
        @.body.pop()

  init : ->

    @lose = false

    if @type == 'right'

      @direction = 'right'

      @body = [
        [2, parseInt(grid[1]/2) - 1]
        [1, parseInt(grid[1]/2) - 1]
        [0, parseInt(grid[1]/2) - 1]
      ]

    else

      @direction = 'left'

      @body = [
        [grid[0] - 2, parseInt(grid[1]/2) - 1]
        [grid[0] - 1, parseInt(grid[1]/2) - 1]
        [grid[0] , parseInt(grid[1]/2) - 1]
      ]

    snakes.push(@)

green = new Snake('red','right','red')

green.init()

white = new Snake('blue','left','blue')

white.init()

$('body').keydown (e) ->
  code = e.keyCode
  for snake in snakes
    snake.bindDirection(code)

timeout = null

over = (str) ->
  clearInterval timeout
  alert str 
  true

clock = ->
  losers = []
  drawFood()
  for snake in snakes
    snake.draw()
    snake.run()

  for snake in snakes
    snake.check()
    if snake.lose
      losers.push snake.name

  if losers.length == 1
    over losers[0] + ' is Lose!'
    return
  if losers.length == 2
    over 'Draw'
    return    
      
  timeout = setTimeout(clock ,speed)

clock()