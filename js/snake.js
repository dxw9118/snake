(function() {
  var Snake, canvas, clock, ctx, drawFood, food, green, grid, over, scale, setFood, size, snakes, speed, timeout, toPos, white;

  size = [500, 500];

  grid = [50, 50];

  scale = size[0] / grid[0];

  snakes = [];

  speed = 100;

  toPos = function(position) {
    return [position[0] * scale, position[1] * scale];
  };

  food = null;

  setFood = function() {
    return food = [parseInt(Math.random() * grid[0]), parseInt(Math.random() * grid[1])];
  };

  canvas = document.getElementsByTagName('canvas')[0];

  ctx = canvas.getContext('2d');

  drawFood = function() {
    var _foot;
    ctx.clearRect(0, 0, size[0], size[1]);
    ctx.fillStyle = '#000';
    _foot = toPos(food);
    return ctx.fillRect(_foot[0], _foot[1], scale, scale);
  };

  setFood();

  Snake = (function() {
    function Snake(name, type, color) {
      this.name = name;
      this.type = type;
      this.color = color;
    }

    Snake.prototype.bindDirection = function(code) {
      if (this.type === 'right') {
        this.keys = [83, 65, 87, 68];
      } else {
        this.keys = [40, 37, 38, 39];
      }
      if (code === this.keys[0] && this.directionPre !== "up") {
        return this.direction = "down";
      } else if (code === this.keys[1] && this.directionPre !== "right") {
        return this.direction = "left";
      } else if (code === this.keys[2] && this.directionPre !== "down") {
        return this.direction = "up";
      } else if (code === this.keys[3] && this.directionPre !== "left") {
        return this.direction = "right";
      }
    };

    Snake.prototype.draw = function() {
      var i, val, _i, _len, _ref, _results, _val;
      ctx.fillStyle = this.color;
      _ref = this.body;
      _results = [];
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        val = _ref[i];
        _val = toPos(val);
        _results.push(ctx.fillRect(_val[0], _val[1], scale, scale));
      }
      return _results;
    };

    Snake.prototype.check = function() {
      var i, snake, val, _i, _len, _results, _snake;
      _snake = this.body[0];
      if (_snake[0] >= 0 && _snake[0] < grid[0] && _snake[1] >= 0 && _snake[1] < grid[0]) {
        _results = [];
        for (_i = 0, _len = snakes.length; _i < _len; _i++) {
          snake = snakes[_i];
          _results.push((function() {
            var _j, _len1, _ref, _results1;
            _ref = snake.body;
            _results1 = [];
            for (i = _j = 0, _len1 = _ref.length; _j < _len1; i = ++_j) {
              val = _ref[i];
              if ((snake.name !== this.name || i > 0) && _snake[0] === val[0] && _snake[1] === val[1]) {
                _results1.push(this.lose = true);
              } else {
                _results1.push(void 0);
              }
            }
            return _results1;
          }).call(this));
        }
        return _results;
      } else {
        return this.lose = true;
      }
    };

    Snake.prototype.eatFood = function() {
      return setFood();
    };

    Snake.prototype.run = function() {
      var _step;
      if (this.direction === 'up') {
        _step = [this.body[0][0], this.body[0][1] - 1];
      } else if (this.direction === 'right') {
        _step = [this.body[0][0] + 1, this.body[0][1]];
      } else if (this.direction === 'down') {
        _step = [this.body[0][0], this.body[0][1] + 1];
      } else if (this.direction === 'left') {
        _step = [this.body[0][0] - 1, this.body[0][1]];
      }
      if (this.directionPre !== this.direction) {
        this.directionPre = this.direction;
      }
      this.body.unshift(_step);
      if (this.body[0][0] === food[0] && this.body[0][1] === food[1]) {
        return this.eatFood();
      } else {
        return this.body.pop();
      }
    };

    Snake.prototype.init = function() {
      this.lose = false;
      if (this.type === 'right') {
        this.direction = 'right';
        this.body = [[2, parseInt(grid[1] / 2) - 1], [1, parseInt(grid[1] / 2) - 1], [0, parseInt(grid[1] / 2) - 1]];
      } else {
        this.direction = 'left';
        this.body = [[grid[0] - 2, parseInt(grid[1] / 2) - 1], [grid[0] - 1, parseInt(grid[1] / 2) - 1], [grid[0], parseInt(grid[1] / 2) - 1]];
      }
      return snakes.push(this);
    };

    return Snake;

  })();

  green = new Snake('red', 'right', 'red');

  green.init();

  white = new Snake('blue', 'left', 'blue');

  white.init();

  $('body').keydown(function(e) {
    var code, snake, _i, _len, _results;
    code = e.keyCode;
    _results = [];
    for (_i = 0, _len = snakes.length; _i < _len; _i++) {
      snake = snakes[_i];
      _results.push(snake.bindDirection(code));
    }
    return _results;
  });

  timeout = null;

  over = function(str) {
    clearInterval(timeout);
    alert(str);
    return true;
  };

  clock = function() {
    var losers, snake, _i, _j, _len, _len1;
    losers = [];
    drawFood();
    for (_i = 0, _len = snakes.length; _i < _len; _i++) {
      snake = snakes[_i];
      snake.draw();
      snake.run();
    }
    for (_j = 0, _len1 = snakes.length; _j < _len1; _j++) {
      snake = snakes[_j];
      snake.check();
      if (snake.lose) {
        losers.push(snake.name);
      }
    }
    if (losers.length === 1) {
      over(losers[0] + ' is Lose!');
      return;
    }
    if (losers.length === 2) {
      over('Draw');
      return;
    }
    return timeout = setTimeout(clock, speed);
  };

  clock();

}).call(this);
